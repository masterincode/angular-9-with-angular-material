import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewRowsDynamicallyComponent } from './add-new-rows-dynamically.component';

describe('AddNewRowsDynamicallyComponent', () => {
  let component: AddNewRowsDynamicallyComponent;
  let fixture: ComponentFixture<AddNewRowsDynamicallyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewRowsDynamicallyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewRowsDynamicallyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
