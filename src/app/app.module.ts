import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { GeneratePasswordComponent } from './component/bootcamp/generate-password/generate-password.component';
import { ParentCardComponent } from './component/bootcamp/parent-card/parent-card.component';
import { ChildCardComponent } from './component/bootcamp/parent-card/child-card/child-card.component';
import { ReactiveFormCodingComponent } from './component/sahosoft/reactive-form-coding/reactive-form-coding.component';
import { MenuBarComponent } from './component/menu-bar/menu-bar.component';
import { HomePageComponent } from './component/sahosoft/home-page/home-page.component';
import { LoginFormComponent } from './component/sahosoft/login-form/login-form.component';
import { NestedFormArrayComponent } from './component/sahosoft/nested-form-array/nested-form-array.component';
import { SiUnitConverterComponent } from './component/bootcamp/si-unit-converter/si-unit-converter.component';
import { ConvertunitPipe } from './pipes/convertunit.pipe';




@NgModule({
  declarations: [
    AppComponent,
    GeneratePasswordComponent,
    ParentCardComponent,
    ChildCardComponent,
    ReactiveFormCodingComponent,
    MenuBarComponent,
    HomePageComponent,
    LoginFormComponent,
    NestedFormArrayComponent,
    SiUnitConverterComponent,
    ConvertunitPipe,
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
