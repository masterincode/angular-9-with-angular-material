import { Injectable } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UserProfileFormGroupService {

// ****************************Form Array Example*****************************************

  formArray = new FormArray([
    new FormControl(), // at 0 index
    new FormControl()  // at 1  index
  ]);

  constructor(private formBuilder: FormBuilder) {}


formArrayExample():void {
    this.formArray.patchValue(['Master In']);
    console.info(`Value is =[${this.formArray.value}] and Status is =${this.formArray.status} `);
    this.formArray.patchValue(['Master In' , 'Code']);
    console.info(`patch Value is = [${this.formArray.value}] and Status is =${this.formArray.status} `);
    this.formArray.reset(['Sachin','Tendulkar']);
    console.info(`Reset Value is =[${this.formArray.value}] and Status is =${this.formArray.status} `);
    this.formArray.removeAt(1);
    console.info(`Remove at 1 Value is =[${this.formArray.value}] and Status is =${this.formArray.status} `);
    // this.formArray.setValue(['Virat','Kohli']);
    // console.info(`Set Value is =[${this.formArray.value}] and Status is =${this.formArray.status} `);

// ===========FormArray====valueChanges and statusChanges returns Observables ======================
    this.formArray.valueChanges.subscribe( (value)=>{
        console.log(value);
    });

    this.formArray.statusChanges.subscribe( (value)=>{
      console.log(value);
    });
    
}



  /* FormGroup has collection of FormControl name as key */

   //============Approach 1=============================================
   userProfileForm = new FormGroup({
//    key    :   value  mapping
    firstName: new FormControl('Master In', [Validators.required , Validators.maxLength(15)]),
    lastName: new FormControl('Code', [Validators.required , Validators.maxLength(15)]),
    age: new FormControl('28', [ Validators.required,Validators.maxLength(2)]),
    email: new FormControl('masterInCode@gmail.com',[ Validators.required, Validators.email ]),
    termsConditions: new FormControl('', Validators.required),
    persontype: new FormControl('',Validators.required),
    technology: new FormControl('Karma Framework', Validators.required)
  });

   // ========Approach 2======Using FormBuilder=======================================
    userProfileFormBuilder = this.formBuilder.group({
      //    key    :   value  mapping
    firstName: ['Master In', [Validators.required , Validators.maxLength(15)] ],
    lastName:['Code', [ Validators.required,Validators.maxLength(15)] ],
    age: ['25',[ Validators.required,Validators.maxLength(2)] ],
    email: ['masterInCode@gmail.com', [ Validators.required, Validators.email ] ],
    termsConditions: ['',Validators.required],
    persontype: ['',Validators.required],
    technology: ['Hibernate',Validators.required]
  });

 }
